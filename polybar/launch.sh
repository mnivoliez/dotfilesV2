#!/usr/bin/env bash

# Launch Polybar with selected style
launch_bar() {
	if [[ ! $(pidof polybar) ]]; then
		polybar -q bar -c "$HOME"/.config/polybar/config.ini &
	else
		polybar-msg cmd restart
	fi
}

# Execute functions
launch_bar
