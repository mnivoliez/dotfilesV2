local function treesitter_setup()
  require 'nvim-treesitter.configs'.setup {
    ensure_installed = "all", -- Only use parsers that are maintained
    sync_install = false,
    auto_install = true,
    ignore_install = { "phpdoc" }, -- https://github.com/claytonrcarter/tree-sitter-phpdoc/issues/15
    highlight = { -- enable highlighting
      enable = true
    },
  }
end

local function lsp_setup()
  require("mason").setup({
    ui = {
      icons = {
        package_installed = "✓",
        package_pending = "➜",
        package_uninstalled = "✗"
      }
    }
  })

  require("mason-lspconfig").setup {
    ensure_installed = { "lua_ls", "rust_analyzer" },
  }

  local coq = require "coq"

  -- tell the server the capability of foldingRange
  -- nvim hasn't added foldingRange to default capabilities, users must add it manually
  local capabilities = vim.lsp.protocol.make_client_capabilities()
  capabilities.textDocument.foldingRange = {
    dynamicRegistration = false,
    lineFoldingOnly = true
  }

  require("mason-lspconfig").setup_handlers {
    -- The first entry (without a key) will be the default handler
    -- and will be called for each installed server that doesn't have
    -- a dedicated handler.
    function(server_name) -- default handler (optional)
      local opts = {
        capabilities = capabilities
      }
      require("lspconfig")[server_name].setup {
        coq.lsp_ensure_capabilities(opts)
      }
    end,
    ["lua_ls"] = function()
      local opts = {
        capabilities = capabilities
      }
      require 'lspconfig'.lua_ls.setup {
        settings = {
          Lua = {
            runtime = {
              -- Tell the language server which version of Lua you're using (most likely LuaJIT in the case of Neovim)
              version = 'LuaJIT',
            },
            diagnostics = {
              -- Get the language server to recognize the `vim` global
              globals = { 'vim', 'use' },
            },
            workspace = {
              -- Make the server aware of Neovim runtime files
              library = vim.api.nvim_get_runtime_file("", true),
            },
            -- Do not send telemetry data containing a randomized but unique identifier
            telemetry = {
              enable = false,
            },
          },
        },
        coq.lsp_ensure_capabilities(opts)
      }
    end
  }
  vim.cmd [[let g:cmake_link_compile_commands = 1]]
end

local function dap_setup()
  local dap              = require('dap')
  dap.adapters.lldb      = {
    type = 'executable',
    command = '/usr/bin/lldb-vscode', -- adjust as needed, must be absolute path
    name = 'lldb'
  }
  dap.adapters.codelldb  = {
    type = 'server',
    port = "${port}",
    executable = {
      -- CHANGE THIS to your path!
      command = vim.fn.stdpath("data") .. '/mason/bin/codelldb',
      args = { "--port", "${port}" },

      -- On windows you may have to uncomment this:
      -- detached = false,
    }
  }
  dap.configurations.cpp = {
    {
      name = 'Launch',
      type = 'codelldb',
      request = 'launch',
      program = function()
        return vim.fn.input('Path to executable: ', vim.fn.getcwd() .. '/', 'file')
      end,
      cwd = '${workspaceFolder}',
      stopOnEntry = false,
      args = {},

      -- 💀
      -- if you change `runInTerminal` to true, you might need to change the yama/ptrace_scope setting:
      --
      --    echo 0 | sudo tee /proc/sys/kernel/yama/ptrace_scope
      --
      -- Otherwise you might get the following error:
      --
      --    Error on launch: Failed to attach to the target process
      --
      -- But you should be aware of the implications:
      -- https://www.kernel.org/doc/html/latest/admin-guide/LSM/Yama.html
      -- runInTerminal = false,
    },
  }

  dap.configurations.c   = dap.configurations.cpp

  local dapui            = require("dapui")
  dapui.setup()

  dap.listeners.after.event_initialized["dapui_config"] = function()
    dapui.open()
  end
  dap.listeners.before.event_terminated["dapui_config"] = function()
    dapui.close()
  end
  dap.listeners.before.event_exited["dapui_config"] = function()
    dapui.close()
  end
end

treesitter_setup()
lsp_setup()
dap_setup()
