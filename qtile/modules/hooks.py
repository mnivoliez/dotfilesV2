from libqtile import hook
from libqtile import qtile
from libqtile.config import Match
import os
import subprocess
from .state import State

state = State()

@hook.subscribe.startup_once
def autostart():
    home = os.path.join(os.environ["XDG_CONFIG_HOME"], "qtile", "autostart.sh") 
    subprocess.Popen([home])

@hook.subscribe.layout_change
@hook.subscribe.client_new
@hook.subscribe.changegroup
@hook.subscribe.focus_change
async def _(*args):
    state.update_state()

ENPASS_MATCH = Match(wm_class="enpass") | Match(wm_class="Enpass") | Match(wm_class="Enpass Assistant")

@hook.subscribe.client_new
def move_client_to_current_group(client):
    if client.match(ENPASS_MATCH):
        client.togroup(qtile.current_group)
    if client.match(Match(wm_class="discover-overlay")):
        client.cmd_bring_to_front()
