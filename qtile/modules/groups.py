from libqtile import layout 
from libqtile.lazy import lazy
from libqtile.config import Group, Match, ScratchPad, DropDown 
from .keys import *


groups = []

# FOR QWERTY KEYBOARDS
group_names = ["1", "2", "3", "4", "5", "6", "7", "8", "9"]

# FOR AZERTY KEYBOARDS
#group_names = ["ampersand", "eacute", "quotedbl", "apostrophe", "parenleft", "minus", "egrave", "underscore", "ccedilla"]

group_labels = ["","","","","","","","",""]
#group_labels = ["Web", "Terminal", "Files", "Code", "Build", "Game", "Mail", "Chat", "Music",]

group_layouts = ["monadtall", "monadtall", "monadtall", "monadtall", "monadtall", "monadtall", "monadtall", "monadtall", "monadtall",]
group_matches = [
        [Match(wm_class=["firefox", "Navigator", "firefoxdeveloperedition"])],
        [],
        [],
        [],
        [],
        [Match(wm_class=["Steam", "steam", "steamwebhelper", "heroic"])],
        [Match(wm_class=["Mail", "thunderbird"])],
        [Match(wm_class=["franz", "Franz", "discord", "discord"])],
        [Match(wm_class=["spotify", "Spotify"])],
    ]
for i in range(len(group_names)):
    groups.append(
        Group(
            name=group_names[i], 
            label=group_labels[i], 
            matches=group_matches[i],
            layout=group_layouts[i].lower(),
        )
    )

for i in groups:
    keys.extend(
        [
            # mod1 + letter of group = switch to group
            Key([mod], i.name, lazy.group[i.name].toscreen(), desc="Switch to group {}".format(i.label)),
            # mod1 + shift + letter of group = switch to & move focused window to group
            Key([mod, "shift"], i.name, lazy.window.togroup(i.name, switch_group=True), desc="Switch to & move focused window to group {}".format(i.label)),
            # Or, use below if you prefer not to switch to that group.
            # # mod1 + shift + letter of group = move focused window to group
            # Key([mod, "shift"], i.name, lazy.window.togroup(i.name),
            #     desc="move focused window to group {}".format(i.name)),
        ]
    )


groups.append(
    ScratchPad('scratchpad', [
        DropDown("term", "alacritty"),
        DropDown("mixer", "alacritty -e pulsemixer"),
        DropDown("enpass", "/opt/enpass/Enpass showassistant"),
]))

keys.extend(
    [
        Key([], "F12", lazy.group["scratchpad"].dropdown_toggle("term")),
        Key([mod, "shift"], "a", lazy.group["scratchpad"].dropdown_toggle("mixer")),
        Key([mod], "e", lazy.group["scratchpad"].dropdown_toggle("enpass"))
    ]
)

