from libqtile import bar
from qtile_extras import widget
from qtile_extras.widget.decorations import RectDecoration
from qtile_extras.widget.decorations import PowerLineDecoration
from .vars import *
from .colors import *

widget_defaults = dict(
    font="Jetbrains Mono Nerd Bold",
    fontsize=12,
    padding=12,
)
extension_defaults = widget_defaults.copy()

def rectdeco(hexcolor,corner=None): #kesekki iwa
	return RectDecoration(
		filled=True, colour=hexcolor,padding=5,
		radius=corner if corner is not None else 0)	

widget_list = [
    widget.Image(
        margin = 3,
        padding = 0,
        filename = '/usr/share/icons/Moka/256x256@2x/places/archlinux.png',
        mouse_callbacks = {"Button1": lazy.spawn(app_launcher)},
        decorations=[rectdeco(background_alt)]
    ),    
    widget.GroupBox(
        this_current_screen_border=blue,
        active=green,
        inactive=gray,
        urgent_border=red,
        highlight_method='text', # box, text, block, line
        disable_drag=True,
        decorations=[rectdeco(background_alt,8)]
    ),
    widget.Spacer(),
    widget.WindowName(
        foreground = foreground,
        decorations=[rectdeco(background_alt,8)]
    ),
    widget.Spacer(),
    widget.PulseVolume(
        foreground = foreground,
        emoji=True,
        decorations=[rectdeco(yellow,8)]
    ),
    widget.Bluetooth(
        foreground = black,
        default_show_battery=True,
        decorations=[rectdeco(orange,8)]
    ),
    widget.Clock(
        foreground = foreground,
        format="    %a, %d %b   ",
        decorations=[rectdeco(green,8)]
    ),
    widget.Clock(  
        foreground=black, 
        format="   %H:%M   ",        
        font = "FontAwesome",
        decorations=[rectdeco(blue,8)],
    ),
    widget.WidgetBox(
        fmt= ' {} ', 
        foreground=foreground, 
        text_closed='', text_open='', 
        close_button_location='right',
        widgets=[widget.Systray()], 
        decorations=[rectdeco(red,8)],
    ),
    widget.TextBox(
        text = "  ",
        font = "FontAwesome",
        foreground = red,
        mouse_callbacks = {"Button1": lazy.spawn(power_menu)},
        decorations=[rectdeco(background_alt,8)]
    ),

]
