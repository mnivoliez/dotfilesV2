require("global")
require("keymaps")
require("plugins")
require("colorschemes")
require("plugin_keymaps")
require("lsp_setup")
vim.cmd[[:COQnow -s]]
