import json
from pathlib import Path

from libqtile import qtile


class State:
    def __init__(self) -> None:
        self.logfile = Path("/tmp/.qtile-state")
        self.logfile.touch()
        self.layouts = {}

    def match_icon(self, window):
        for k, v in self.icons["icons"].items():
            if k in window or k == window:
                return v

    def get_layout(self, layout, windows):
        if layout == "max":
            return f"max ({len(windows)} )" if len(windows) > 1 else "max"
        else:
            return layout

    def get_group_state(self, name, info, index):
        g_state = {"name": name}
        g_state["label"] = info["label"]
        g_state["windows"] = info["windows"] 
        if info["screen"] is not None:
            if info["screen"] == index:
                g_state["status"] = "active"
                self.layouts[index] = self.get_layout(info["layout"], info["windows"])
            else:
                g_state["status"] = "busy"

        return g_state

    def get_state(self, update={}):
        screens = qtile.get_screens()
        groups = qtile.get_groups()
        screen_state = {}

        for s in screens:
            screen_state[s["index"]] = {"groups": [], "layout": ""}
            for g, info in groups.items():
                if g == "scratchpad":
                    continue

                g_state = self.get_group_state(g, info, s["index"])
                screen_state[s["index"]]["groups"].append(g_state)

            screen_state[s["index"]]["layout"] = self.layouts[s["index"]]

        self.state = {"screens": screen_state, "chord": "", **update}

    def write_state(self):
        with self.logfile.open("a") as f:
            f.write(json.dumps(self.state) + "\n")

    def update_state(self, update={}):
        self.get_state(update)
        self.write_state()
