#!/bin/bash

#                __             __             __
#   ____ ___  __/ /_____  _____/ /_____ ______/ /_
#  / __ `/ / / / __/ __ \/ ___/ __/ __ `/ ___/ __/
# / /_/ / /_/ / /_/ /_/ (__  ) /_/ /_/ / /  / /_
# \__,_/\__,_/\__/\____/____/\__/\__,_/_/   \__/
#

$HOME/.config/polybar/launch.sh &

# Restart sxhkd
pgrep -x sxhkd | xargs kill
sleep 0.5
sxhkd -c $HOME/.config/sxhkd/sxhkdrc -m -1 &

# Restart default apps
declare -a restart=(
  picom
  dunst
  /usr/lib/geoclue-2.0/demos/agent
  flameshot
  enpass
  discord
  discover-overlay
  franz
  birdtray
  owncloud
  qbittorrent
  "playerctld daemon"
)
for i in "${restart[@]}"; do
	pgrep -x "$i" | xargs kill
  sleep 0.5
	eval "$i" &
done

# Exclusive apps
if [[ ! $(pidof stalonetray) ]]; then
	stalonetray &
  sleep 0.5
	xdo hide -N stalonetray
	touch "/tmp/syshide.lock"
fi

dunstify -i window_list "BSPWM" "Completed autostarting all apps"
