import os
from libqtile.lazy import lazy

colors = []
cache=os.path.expanduser('~/.cache/wal/colors')
def load_colors(cache):
    with open(cache, 'r') as file:
        for i in range(16):
            colors.append(file.readline().strip())
    colors.append('#ffffff')
    lazy.reload_config()
load_colors(cache)

# Start flavours
transparent = '#00000000'
background  = '#211f1f'
foreground  = '#c4bfb7'

black       = '#382119'
red         = '#b5371c'
green       = '#6c6c4c'
yellow      = '#efb150'
blue        = '#557f89'
magenta     = '#b3b08a'
cyan        = '#6ea497'
orange      = '#e76f1b'
white       = '#ebeae3'
gray        = '#5b4840'
graylight   = '#a1978f'
# End flavours
background_alt = '#14141499'

