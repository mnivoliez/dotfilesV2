#!/bin/bash

#                __             __             __
#   ____ ___  __/ /_____  _____/ /_____ ______/ /_
#  / __ `/ / / / __/ __ \/ ___/ __/ __ `/ ___/ __/
# / /_/ / /_/ / /_/ /_/ (__  ) /_/ /_/ / /  / /_
# \__,_/\__,_/\__/\____/____/\__/\__,_/_/   \__/
#

#$HOME/.config/polybar/launch.sh &

feh --bg-scale $HOME/.walls/wall.png
wal -i $HOME/.walls/wall.png

# Restart default apps
declare -a restart=(
  "/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1"
  picom
  dunst
  /usr/lib/geoclue-2.0/demos/agent
  flameshot
  blueman-applet
  enpass
  discord
  discover-overlay
  franz
  birdtray
  nextcloud
  qbittorrent
  "playerctld daemon"
  "caffeine start"
)
for i in "${restart[@]}"; do
	pgrep -x "$i" | xargs kill
  sleep 0.5
	eval "$i" &
done

dunstify -i window_list "QTile" "Completed autostarting all apps"
